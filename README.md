# Apex Lifeline Service

Medical Drone REST API service

### How to run
System requires an installation of java
Can be run via CLI gradle wrapper\
`./gradlew bootRun`

### Register drone 
`curl --location --request POST 'localhost:8080/v1/drones/create' \
--header 'Content-Type: application/json' \
--data-raw '
{
"serialNumber": "1o003",
"model": "HEAVYWEIGHT",
"weightLimitGrams": 50,
"battery": 20
}'`

### Create medication
`curl --location --request POST 'localhost:8080/v1/medication/create' \
--form 'name="zenma"' \
--form 'weightInGrams="25"' \
--form 'code="AZ_1K5"' \
--form 'image=@"/Users/fedache/Downloads/ballon_contender_1.jpg"'`

### Load drone
`curl --location --request POST 'localhost:8080/v1//drones/load' \
--header 'Content-Type: application/json' \
--data-raw '{
"droneId": 2,
"medications": [
1, 2
]
}'`

### Drone Status
`curl --location --request GET 'localhost:8080/v1//drones/status/1' \
--header 'Content-Type: application/json' \
--data-raw '{
"droneId": 2,
"medications": [
1, 2
]
}'`

### Available Drones
`curl --location --request GET 'localhost:8080/v1/drones/statusAvailable?page=1' \
--data-raw ''`


### Known quirks
- No swagger docs on this, but here are sample requests
- No docker container
- No external logging service or database
- No Security / session / cache infastructure