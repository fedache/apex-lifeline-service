package com.musala.apexlifeline.entity;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Set;

@Entity
public class Drone {
    @Id
    @GeneratedValue
    private Long id;

    private String serialNumber;
    @Enumerated(EnumType.STRING)
    private Model model;
    private Double weightLimitGrams;

    private Integer battery;

    @NotNull
    @Enumerated(EnumType.STRING)
    private State state = State.IDLE;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Medication> medications;

    public Drone() {
    }

    public Drone(String serialNumber, Model model, Double weightLimitGrams, Integer battery) {
        this.serialNumber = serialNumber;
        this.model = model;
        this.weightLimitGrams = weightLimitGrams;
        this.battery = battery;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public Double getWeightLimitGrams() {
        return weightLimitGrams;
    }

    public void setWeightLimitGrams(Double weightLimitGrams) {
        this.weightLimitGrams = weightLimitGrams;
    }

    public Integer getBattery() {
        return battery;
    }

    public void setBattery(Integer battery) {
        this.battery = battery;
    }

    public List<Medication> getMedications() {
        return medications;
    }

    public void setMedications(List<Medication> medications) {
        this.medications = medications;
    }

    public enum State {
        IDLE,
        LOADING,
        LOADED,
        DELIVERING,
        DELIVERED,
        RETURNING
    }

    public enum Model {
        LIGHTWEIGHT,
        MIDDLEWEIGHT,
        CRUISERWEIGHT,
        HEAVYWEIGHT
    }

    @Override
    public String toString() {
        return "Drone{" +
                "id=" + id +
                ", serialNumber='" + serialNumber + '\'' +
                ", model=" + model +
                ", weightLimitGrams=" + weightLimitGrams +
                ", battery=" + battery +
                ", state=" + state +
                '}';
    }
}
