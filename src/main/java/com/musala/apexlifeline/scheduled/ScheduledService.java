package com.musala.apexlifeline.scheduled;

import com.musala.apexlifeline.entity.Drone;
import com.musala.apexlifeline.service.DroneDispatchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@EnableAsync
@Service
public class ScheduledService {
    private final DroneDispatchService droneDispatchService;
    private final Logger logger = Logger.getLogger("ScheduledService");

    @Autowired
    public ScheduledService(DroneDispatchService droneDispatchService) {
        this.droneDispatchService = droneDispatchService;
    }

    @Async
    @Scheduled(fixedRate = 5_000)
    public void scheduleBatteryLevels() {
        List<Drone> allDrones = droneDispatchService.findAllDrones();
        for (Drone allDrone : allDrones) {
            logger.log(Level.INFO, String.format("%s", allDrone.toString()));
        }
    }
}