package com.musala.apexlifeline;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class ApexLifelineApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApexLifelineApplication.class, args);
    }

}
