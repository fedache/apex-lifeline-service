package com.musala.apexlifeline.config;

import com.musala.apexlifeline.dto.DroneDTO;
import com.musala.apexlifeline.dto.MedicationDTO;
import com.musala.apexlifeline.entity.Drone;
import com.musala.apexlifeline.service.DroneDispatchService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.util.logging.Logger;

@Configuration
@Profile("!unitTest")
public class PreloadDatabase {
    private final Logger logger = Logger.getLogger("InitDatabase");

    @Bean
    CommandLineRunner initDatabase(DroneDispatchService service) {
        return args -> {
            logger.info(String.format("Preloading %s", service.registerDrone(new DroneDTO("SK-SSD", Drone.Model.CRUISERWEIGHT, 100.9, 45))));
            logger.info(String.format("Preloading %s", service.registerDrone(new DroneDTO("BBS-23", Drone.Model.LIGHTWEIGHT, 50.9, 70))));
            logger.info(String.format("Preloading %s", service.registerDrone(new DroneDTO("BBS-24", Drone.Model.CRUISERWEIGHT, 10.9, 90))));
            logger.info(String.format("Preloading %s", service.registerDrone(new DroneDTO("BBS-25", Drone.Model.LIGHTWEIGHT, 70.9, 31))));
            logger.info(String.format("Preloading %s", service.registerDrone(new DroneDTO("BBS-26", Drone.Model.LIGHTWEIGHT, 90.9, 45))));
            logger.info(String.format("Preloading %s", service.registerDrone(new DroneDTO("BBS-27", Drone.Model.MIDDLEWEIGHT, 200.9, 5))));
            logger.info(String.format("Preloading %s", service.registerDrone(new DroneDTO("BBS-28", Drone.Model.LIGHTWEIGHT, 250.9, 100))));
            logger.info(String.format("Preloading %s", service.registerDrone(new DroneDTO("BBS-29", Drone.Model.CRUISERWEIGHT, 150.9, 45))));
            logger.info(String.format("Preloading %s", service.registerDrone(new DroneDTO("BBS-30", Drone.Model.LIGHTWEIGHT, 250.9, 92))));
            logger.info(String.format("Preloading %s", service.registerDrone(new DroneDTO("BBS-31", Drone.Model.HEAVYWEIGHT, 250.9, 12))));

            logger.info(String.format("Preloading %s", service.createMedication(new MedicationDTO("medik12", 10.0, "MEDL_12"), null)));
            logger.info(String.format("Preloading %s", service.createMedication(new MedicationDTO("medik13", 9.2, "MEDL_13"), null)));
            logger.info(String.format("Preloading %s", service.createMedication(new MedicationDTO("medik15", 3.2, "MEDL_15"), null)));
            logger.info(String.format("Preloading %s", service.createMedication(new MedicationDTO("medik19", 33.2, "MEDL_19"), null)));
        };
    }
}
