package com.musala.apexlifeline.dto;

import com.musala.apexlifeline.entity.Drone;
import org.hibernate.validator.constraints.Length;
import org.springframework.lang.Nullable;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class DroneDTO {
    @NotEmpty
    @Length(max = 100)
    private String serialNumber;
    @NotNull
    private Drone.Model model;

    @Min(0)
    @Max(500)
    private Double weightLimitGrams;

    @Nullable
    @Max(100)
    private Integer battery;

    public DroneDTO(String serialNumber, Drone.Model model, Double weightLimitGrams, @Nullable Integer battery) {
        this.serialNumber = serialNumber;
        this.model = model;
        this.weightLimitGrams = weightLimitGrams;
        this.battery = battery;
    }

    public DroneDTO() {
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public Drone.Model getModel() {
        return model;
    }

    public void setModel(Drone.Model model) {
        this.model = model;
    }

    public Double getWeightLimitGrams() {
        return weightLimitGrams;
    }

    public void setWeightLimitGrams(Double weightLimitGrams) {
        this.weightLimitGrams = weightLimitGrams;
    }

    public Integer getBattery() {
        return battery;
    }

    public void setBattery(Integer battery) {
        this.battery = battery;
    }

    public Drone toDrone() {
        return new Drone(serialNumber, model, weightLimitGrams, battery);
    }
}
