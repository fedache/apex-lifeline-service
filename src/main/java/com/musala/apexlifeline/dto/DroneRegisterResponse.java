package com.musala.apexlifeline.dto;

import com.musala.apexlifeline.entity.Drone;

public class DroneRegisterResponse {
    private String message;
    private Drone data;

    public DroneRegisterResponse() {
    }

    public DroneRegisterResponse(String message, Drone data) {
        this.message = message;
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Drone getData() {
        return data;
    }

    public void setData(Drone data) {
        this.data = data;
    }
}
