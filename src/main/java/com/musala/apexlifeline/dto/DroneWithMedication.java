package com.musala.apexlifeline.dto;

import java.util.List;

public class DroneWithMedication {
    private Long droneId;
    private List<Long> medications;

    public DroneWithMedication() {
    }

    public Long getDroneId() {
        return droneId;
    }

    public void setDroneId(Long droneId) {
        this.droneId = droneId;
    }

    public List<Long> getMedications() {
        return medications;
    }

    public void setMedications(List<Long> medications) {
        this.medications = medications;
    }
}
