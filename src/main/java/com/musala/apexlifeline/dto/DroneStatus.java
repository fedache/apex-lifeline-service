package com.musala.apexlifeline.dto;

import com.musala.apexlifeline.entity.Drone;
import com.musala.apexlifeline.entity.Medication;

import java.util.List;

public class DroneStatus {
    private Integer battery;
    private Drone.State state;
    private List<Medication> medications;

    public DroneStatus() {
    }

    public DroneStatus(Integer battery, Drone.State state, List<Medication> medications) {
        this.battery = battery;
        this.state = state;
        this.medications = medications;
    }

    public Integer getBattery() {
        return battery;
    }

    public void setBattery(Integer battery) {
        this.battery = battery;
    }

    public Drone.State getState() {
        return state;
    }

    public void setState(Drone.State state) {
        this.state = state;
    }

    public List<Medication> getMedications() {
        return medications;
    }

    public void setMedications(List<Medication> medications) {
        this.medications = medications;
    }
}
