package com.musala.apexlifeline.dto;

import com.musala.apexlifeline.entity.Medication;

import javax.persistence.Column;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

public class MedicationDTO {
    @NotBlank
    @Pattern(regexp = "^[a-zA-Z0-9-_]*$")
    private String name;

    @Min(0)
    private Double weightInGrams;

    @Pattern(regexp = "^[A-Z0-9_]*$")
    private String code;

    public MedicationDTO(String name, Double weightInGrams, String code) {
        this.name = name;
        this.weightInGrams = weightInGrams;
        this.code = code;
    }

    public MedicationDTO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getWeightInGrams() {
        return weightInGrams;
    }

    public void setWeightInGrams(Double weightInGrams) {
        this.weightInGrams = weightInGrams;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Medication toMedication(String imageUrl) {
        return new Medication(name, weightInGrams, code, imageUrl);
    }
}
