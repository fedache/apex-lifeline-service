package com.musala.apexlifeline.controller;

import com.musala.apexlifeline.dto.*;
import com.musala.apexlifeline.entity.Drone;
import com.musala.apexlifeline.entity.Medication;
import com.musala.apexlifeline.service.DroneDispatchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/v1")
public class DispatchController {
    private final DroneDispatchService dispatchService;

    @Autowired
    public DispatchController(DroneDispatchService dispatchService) {
        this.dispatchService = dispatchService;
    }

    @PostMapping(path = "/drones/create", produces = "application/json")
    public BaseResponse<Drone> registerDrone(@RequestBody @Valid DroneDTO drone) {
        return new BaseResponse<>("Drone created successfully", dispatchService.registerDrone(drone));
    }

    @PostMapping(path = "/medication/create", produces = "application/json")
    public BaseResponse<Medication> createMedication(
            @RequestParam("name") String name,
            @RequestParam("weightInGrams") Double weightInGrams,
            @RequestParam("code") String code,
            @RequestParam("image") MultipartFile file) {
        var medication = new MedicationDTO(name, weightInGrams, code);
        return new BaseResponse<>("Medication created successfully", dispatchService.createMedication(medication, file));
    }

    @PostMapping(path = "/drones/load", produces = "application/json")
    public BaseResponse<Drone> loadDrone(@RequestBody DroneWithMedication droneWithMedication) {
        return new BaseResponse<>("Drone loaded", dispatchService.loadDrones(droneWithMedication));
    }

    @GetMapping(path = "/drones/status/{id}", produces = "application/json")
    public BaseResponse<DroneStatus> droneStatus(@PathVariable("id") Long droneId) {
        return new BaseResponse<>("Drone loaded", dispatchService.getDrone(droneId));
    }

    @GetMapping(path = "/drones/statusAvailable", produces = "application/json", params = {"page"})
    public BaseResponse<List<Drone>> droneStatus(@RequestParam(value = "page") int page) {
        var pageSize = 10;
        if (page < 1)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Page should be > 0");
        List<Drone> serialNumber = dispatchService.getAvailableDrones(PageRequest.of(page - 1, pageSize, Sort.by("serialNumber")));
        String message;
        if (serialNumber.isEmpty())
            message = "No Drones are available";
        else
            message = "Drones available";
        return new BaseResponse<>(message, serialNumber);
    }

}
