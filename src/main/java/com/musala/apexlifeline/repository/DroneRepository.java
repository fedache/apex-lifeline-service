package com.musala.apexlifeline.repository;

import com.musala.apexlifeline.entity.Drone;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DroneRepository extends JpaRepository<Drone, Long> {
    List<Drone> findAllByState(Drone.State state, Pageable pageable);
}
