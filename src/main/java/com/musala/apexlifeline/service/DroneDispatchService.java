package com.musala.apexlifeline.service;

import com.musala.apexlifeline.dto.DroneDTO;
import com.musala.apexlifeline.dto.DroneStatus;
import com.musala.apexlifeline.dto.DroneWithMedication;
import com.musala.apexlifeline.dto.MedicationDTO;
import com.musala.apexlifeline.entity.Drone;
import com.musala.apexlifeline.entity.Medication;
import com.musala.apexlifeline.repository.DroneRepository;
import com.musala.apexlifeline.repository.MedicationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@Validated
public class DroneDispatchService {
    private final DroneRepository droneRepository;
    private final MedicationRepository medicationRepository;
    private final BlobStorageService blobStorageService;

    @Autowired
    public DroneDispatchService(DroneRepository droneRepository, MedicationRepository medicationRepository, BlobStorageService blobStorageService) {
        this.droneRepository = droneRepository;
        this.medicationRepository = medicationRepository;
        this.blobStorageService = blobStorageService;
    }

    public Drone registerDrone(@NonNull DroneDTO drone) {
        return droneRepository.save(drone.toDrone());
    }

    public Medication createMedication(@Valid MedicationDTO medication, @Nullable MultipartFile file) {
        String imageUrl = null;
        if (file != null) {
            try {
                imageUrl = blobStorageService.storeFile(String.format("%s-%s", medication.getName(), UUID.randomUUID()), file);
            } catch (IOException e) {
                e.printStackTrace();
                throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Unable to save medication file");
            }
        }
        return medicationRepository.save(medication.toMedication(imageUrl));
    }

    public Drone loadDrones(DroneWithMedication droneWithMedication) {
        var droneOption = droneRepository.findById(droneWithMedication.getDroneId());
        if (droneOption.isEmpty())
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Drone doesn't exist");
        var medications = medicationRepository.findAllById(droneWithMedication.getMedications());
        Drone drone = droneOption.get();
        if (drone.getBattery() < 25) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Battery to low, to be loaded");
        }
        var weightLimit = drone.getWeightLimitGrams();
        if (weightLimit == null) {
            weightLimit = 0.0;
        }
        var currentMedicationWeight = 0.0;
        for (var medication : medications) {
            var medWeight = medication.getWeightInGrams();
            currentMedicationWeight += medWeight;
            if (currentMedicationWeight >= weightLimit) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Weight limit for drone exceeded");
            }
        }
        drone.setMedications(medications);
        drone.setState(Drone.State.LOADED);
        return droneRepository.save(drone);
    }

    public DroneStatus getDrone(Long droneId) {
        Optional<Drone> droneOption = droneRepository.findById(droneId);
        if (droneOption.isEmpty()){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Drone doesn't seem to exist");
        }
        var drone = droneOption.get();
        return new DroneStatus(drone.getBattery(), drone.getState(), drone.getMedications());
    }

    public List<Drone> getAvailableDrones(Pageable pageable) {
        return droneRepository.findAllByState(Drone.State.IDLE, pageable);
    }

    public List<Drone> findAllDrones() {
        return droneRepository.findAll();
    }
}
