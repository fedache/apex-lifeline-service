package com.musala.apexlifeline.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
@Scope("singleton")
public class BlobStorageService {
    private final Path basePath;

    public BlobStorageService(@Value("${blob.storage.path}") String basePath) throws IOException {
        this.basePath = Paths.get(basePath).toAbsolutePath().normalize();
        Files.createDirectories(this.basePath);
    }

    public String storeFile(String name, MultipartFile file) throws IOException {
        String fileName = StringUtils.cleanPath(name);
        Path targetLocation = this.basePath.resolve(fileName);
        Files.copy(file.getInputStream(), targetLocation);
        return fileName;
    }
}
