package com.musala.apexlifeline.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.musala.apexlifeline.dto.DroneDTO;
import com.musala.apexlifeline.dto.MedicationDTO;
import com.musala.apexlifeline.entity.Drone;
import com.musala.apexlifeline.entity.Medication;
import com.musala.apexlifeline.service.DroneDispatchService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.hamcrest.CoreMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@ActiveProfiles("unitTest")
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class DispatchControllerTest {
    @Autowired
    private MockMvc mvc;
    @Autowired
    private DroneDispatchService service;
    @Autowired
    private ObjectMapper mapper;

    @Test
    public void testHappyPathCreateDrone() throws Exception {
        mvc.perform(MockMvcRequestBuilders.post("/v1/drones/create")
                .content("{\n" +
                        "    \"serialNumber\": \"1o003\",\n" +
                        "    \"model\": \"LIGHTWEIGHT\",\n" +
                        "    \"weightLimitGrams\": 22.2,\n" +
                        "    \"battery\": 20\n" +
                        "}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("message", containsString("created")));
    }

    @Test
    public void testUnknownModelCreateDrone() throws Exception {
        mvc.perform(MockMvcRequestBuilders.post("/v1/drones/create")
                .content("{\n" +
                        "    \"serialNumber\": \"1o003\",\n" +
                        "    \"model\": \"WOOW\",\n" +
                        "    \"weightLimitGrams\": 22.2,\n" +
                        "    \"battery\": 20\n" +
                        "}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(not(HttpStatus.OK)));
    }

    @Test
    public void testMaxWeightCreateDrone() throws Exception {
        mvc.perform(MockMvcRequestBuilders.post("/v1/drones/create")
                .content("{\n" +
                        "    \"serialNumber\": \"1o003\",\n" +
                        "    \"model\": \"LIGHTWEIGHT\",\n" +
                        "    \"weightLimitGrams\": 5000,\n" +
                        "    \"battery\": 20\n" +
                        "}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(not(HttpStatus.OK)));
    }

    @Test
    public void testRegisterDroneMedication() throws Exception {
        Drone drone = service.registerDrone(mapper.readValue("{\n" +
                "    \"serialNumber\": \"1o003\",\n" +
                "    \"model\": \"LIGHTWEIGHT\",\n" +
                "    \"weightLimitGrams\": 400,\n" +
                "    \"battery\": 90\n" +
                "}", DroneDTO.class));

        Medication medication = service.createMedication(mapper.readValue("{\n" +
                "    \"name\": \"az-capsule\",\n" +
                "    \"weightInGrams\": \"25\",\n" +
                "    \"code\": \"AZ_1K5\"" +
                "}", MedicationDTO.class), null);

        Medication medication1 = service.createMedication(mapper.readValue("{\n" +
                "    \"name\": \"ms-capsule\",\n" +
                "    \"weightInGrams\": \"63\",\n" +
                "    \"code\": \"MS_1K5\"" +
                "}", MedicationDTO.class), null);

        mvc.perform(MockMvcRequestBuilders.post("/v1/drones/load")
                .contentType(MediaType.APPLICATION_JSON)
                .content(String.format("{\n" +
                        "    \"droneId\": %d,\n" +
                        "    \"medications\": [\n" +
                        "        %d, %d\n" +
                        "    ]\n" +
                        "}", drone.getId(), medication.getId(), medication1.getId()))
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("message", containsString("loaded")));
    }

    @Test
    public void testRegisterDroneMedication__OverWeightLimit() throws Exception {
        Drone drone = service.registerDrone(mapper.readValue("{\n" +
                "    \"serialNumber\": \"1o003\",\n" +
                "    \"model\": \"LIGHTWEIGHT\",\n" +
                "    \"weightLimitGrams\": 78,\n" +
                "    \"battery\": 20\n" +
                "}", DroneDTO.class));

        Medication medication = service.createMedication(mapper.readValue("{\n" +
                "    \"name\": \"az-capsule\",\n" +
                "    \"weightInGrams\": \"25\",\n" +
                "    \"code\": \"AZ_1K5\"" +
                "}", MedicationDTO.class), null);

        Medication medication1 = service.createMedication(mapper.readValue("{\n" +
                "    \"name\": \"ms-capsule\",\n" +
                "    \"weightInGrams\": \"63\",\n" +
                "    \"code\": \"MS_1K5\"" +
                "}", MedicationDTO.class), null);

        mvc.perform(MockMvcRequestBuilders.post("/v1/drones/load")
                .contentType(MediaType.APPLICATION_JSON)
                .content(String.format("{\n" +
                        "    \"droneId\": %d,\n" +
                        "    \"medications\": [\n" +
                        "        %d, %d\n" +
                        "    ]\n" +
                        "}", drone.getId(), medication.getId(), medication1.getId()))
        )
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void testRegisterDroneStatus() throws Exception {
        Drone drone = service.registerDrone(mapper.readValue("{\n" +
                "    \"serialNumber\": \"1o003\",\n" +
                "    \"model\": \"LIGHTWEIGHT\",\n" +
                "    \"weightLimitGrams\": 78,\n" +
                "    \"battery\": 20\n" +
                "}", DroneDTO.class));
        mvc.perform(MockMvcRequestBuilders.get(String.format("/v1/drones/status/%d", drone.getId())))
                .andExpect(status().isOk());
    }

    @Test
    public void testRegisterDroneStatus__NoDrone() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get(String.format("/v1/drones/status/%d", 1)))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void testAvailableDrones() throws Exception {
        Drone drone = service.registerDrone(mapper.readValue("{\n" +
                "    \"serialNumber\": \"cde\",\n" +
                "    \"model\": \"LIGHTWEIGHT\",\n" +
                "    \"weightLimitGrams\": 78,\n" +
                "    \"battery\": 20\n" +
                "}", DroneDTO.class));
        Drone drone1 = service.registerDrone(mapper.readValue("{\n" +
                "    \"serialNumber\": \"abc\",\n" +
                "    \"model\": \"CRUISERWEIGHT\",\n" +
                "    \"weightLimitGrams\": 80,\n" +
                "    \"battery\": 90\n" +
                "}", DroneDTO.class));
        mvc.perform(MockMvcRequestBuilders.get("/v1/drones/statusAvailable?page=1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data[0].serialNumber", is("abc")))
                .andExpect(jsonPath("$.data[1].serialNumber", is("cde")));
    }

}